#!/usr/bin/env python3

import argparse
import gitlab
import csv
import os
import yaml
import re
import warnings

warnings.filterwarnings("ignore", category=DeprecationWarning)

def get_group_variables(gl, topgroup):
    grouplist = [topgroup]
    projects = []
    variables = []
    print("Compiling grouplist")
    get_sub_groups(gl, topgroup, grouplist)
    for group in grouplist:
        print("Crawling variables for %s" % group.full_path)
        projects = get_projects(gl, group)
        try:
            group_variables = group.variables.list(as_list=False)
            for variable in group_variables:
                variable_object = {"type":"Group","path":group.full_path,"key":variable.key,"value":variable.value,"protected":variable.protected,"masked":variable.masked}
                variables.append(variable_object)
        except Exception as e:
            print("Can not get group variables for %s : %s" % (group.full_path, e))

    for project in projects:
        print("Crawling variables for %s" % project.path_with_namespace)
        try:
            project_variables = project.variables.list(as_list=False)
            for variable in project_variables:
                variable_object = {"type":"Project","path":project.path_with_namespace,"key":variable.key,"value":variable.value,"protected":variable.protected,"masked":variable.masked}
                variables.append(variable_object)
        except Exception as e:
            print("Can not get project variables for %s : %s" % (project.path_with_namespace, e))
    return variables

def get_sub_groups(gl, group, grouplist):
    groups = group.subgroups.list(as_list=False)
    for subgroup in groups:
        print("Getting subgroups of %s" % subgroup.full_path)
        subgroup_object = gl.groups.get(subgroup.id)
        grouplist.append(subgroup_object)
        get_sub_groups(gl, subgroup_object, grouplist)

def get_projects(gl, group):
    project_list = []
    projects = group.projects.list(as_list=False)
    for project in projects:
        project_list.append(gl.projects.get(project.id))
    return project_list

def write_report(reportfilepath, users_to_remove):
    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","last_activity_on","groups","projects"]
        reportwriter.writerow(fields)
        for user in users_to_remove:
            row = []
            for field in fields:
                if field == "last_activity_on":
                    if user["id"] in last_events:
                        row.append(last_events[user["id"]])
                    else:
                        row.append("")
                elif field == "groups" or field == "projects":
                    row.append(user[field])
                else:
                    row.append(user[field])
            reportwriter.writerow(row)

parser = argparse.ArgumentParser(description='Find CI variables containing secrets that are not masked')
parser.add_argument('token', help='API token able to read the requested group and projects')
parser.add_argument('group', help='ID or namespace of the group in which to search for unmasked secrets')
parser.add_argument('--gitlaburl', help='Url of the gitlab instance', default="https://gitlab.com/")
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token = args.token)

try:
    group = gl.groups.get(args.group)
except Exception as e:
    print("Can not retrieve group: "+ str(e))
    exit(1)

leaks_rules = []
with open("leaks_rules.yml", "r") as rulesfile:
    leaks_rules = yaml.safe_load(rulesfile)["rules"]

variables = get_group_variables(gl, group)
findings = []

for variable in variables:
    mask_keys = ["secret","key","token","pass","aws","gcp","azure","credential","private","ssh","pgp"]
    mask_string_found = []
    for mask_key in mask_keys:
        if mask_key in variable["key"].lower():
            mask_string_found.append(mask_key)
    if mask_string_found and not variable["masked"]:
        print(variable["type"] + " variable '" + variable["key"] + "' in " + variable["path"] + " should be masked. The key contains " + ", ".join(mask_string_found))
        findings.append([variable["type"], variable["path"], variable["key"], "Key violation", ", ".join(mask_string_found)])

    leak_finding = []
    for rule in leaks_rules:
        result = re.match(rule["regex"].strip(), variable["value"])
        if result and not variable["masked"]:
            print(variable["type"] + " variable '" + variable["key"] + "' in " + variable["path"] + " should be masked. The value looks like a " + rule["description"])
            findings.append([variable["type"], variable["path"], variable["key"], "Value violation", rule["description"]])

with open("unmasked_secrets.csv","w") as outfile:
    writer = csv.writer(outfile, delimiter="\t", quotechar='"')
    fields = ["variable type","path","variable key","violation type","evidence"]
    writer.writerow(fields)
    for finding in findings:
        writer.writerow(finding)
