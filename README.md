# Find unmasked secrets

GitLab offers to configure variables on project and group level via `Settings -> CI/CD -> Variables`. If secrets are stored in these variables, they should be masked.

This script analyzes all CI/CD variables in a group, its subgroups and projects. For each variable:

* If the variables' `key` contains one of `"secret","key","token","pass","aws","gcp","azure","credential","private","ssh","pgp"` AND the variable is not masked, mark it as `key violation`
* If the variables' `value` matches one of the regexes in [leaks_rules.yml](leaks_rules.yml) AND the variable is not masked, mark it as `value violation`

# Running the script

* Fork this project (`visibility: private`)
* Configure an access token with `api` access to the group you want to run on. Create a `CI/CD variable` `GIT_TOKEN` containing the token. Make sure to `mask` it.
* Configure a `CI/CD variable` `GROUP` containing the ID of the group you want to analyze.
* Find the report in the artifacts as `unmasked_secrets.csv`
